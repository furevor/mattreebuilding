import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';


/**
 * Node for to-do item
 */
export class TodoItemNode {
    parent: TodoItemNode;
    children: TodoItemNode[];
    item: string;
    extendable: boolean;

    constructor(item = '', extendable = false, children = undefined, parent = undefined)
    {
        this.parent = parent;
        this.item = item;
        this.extendable = extendable;
        this.children = children;
    }
}

/** Flat to-do item node with expandable and level information */
export class TodoItemFlatNode {
    item: string;
    level: number;
    expandable: boolean;
    extendable: boolean;


}

const mock_users_layers = {
    "_id": {
        "$oid": "5b43725f8992539d029b03b1"
    },
    "properties": {
        "shared": "private",
        "ownerId": {
            "$oid": "5b348a9c39d652520f34bdf9"
        }
    },
    "children": [
        {
            "properties": {
                "_id": {
                    "$oid": "59955f0f39d6525eb55b3493"
                },
                "type": "layerEntity",
                "name": "New layer",
                "index": [],
                "api": "api\/getObj?layer=59955f0f39d6525eb55b3493",
                "children": []
            }
        },
        {
            "_id": {
                "$oid": "5995682839d65232396c4d94"
            }
        },
        {
            "_id": {
                "$oid": "5995683439d65233f66c12a3"
            }
        }
    ]
};

const TREE_DATA = {
    Groceries: {
        'Almond Meal flour': null,
        'Organic eggs': null,
        'Protein Powder': null,
        Fruits: {
            Apple: null,
            Berries: ['Blueberry', 'Raspberry'],
            Orange: null
        }
    },
    Reminders: [
        'Cook dinner',
        'Read the Material Design spec',
        'Organic eggs',
        'Upgrade Application to Angular'
    ]
};

@Injectable({
    providedIn: 'root'
})
export class ChecklistDatabaseService {

    dataChange = new BehaviorSubject<TodoItemNode[]>([]);

    get data(): TodoItemNode[] { return this.dataChange.value; }

    // dataChange = new BehaviorSubject<MenuItemNode[]>([]);
    // layerChange = new BehaviorSubject({});

    // get data(): MenuItemNode[] {
    //     return this.dataChange.value;
    // }

    set data(value: TodoItemNode[]) {
        this.dataChange.value.concat(value);
    }

    json_tree: any;


    constructor(private httpClient: HttpClient) {
        this.initialize();
    }

    initialize() {
        // here's data -> true/false loggedIn status
        // this.authenticationService.getAuthState().subscribe(data => {
        //     console.log('сюда прилетает authState ---> ' + data);
        //     this.getMenu();
        // });

        // console.log('Проверка работы механизма... ' + data);
        // this.getMenu();
        const data = this.buildFileTree(TREE_DATA, 0);

        // Notify the change.
        this.dataChange.next(data);

    }

    /**
     * Build the file structure tree. The `value` is the Json object, or a sub-tree of a Json object.
     * The return value is the list of `TodoItemNode`.
     */
    buildFileTree(obj: object, level: number, parent: TodoItemNode = undefined): TodoItemNode[] {
        return Object.keys(obj).reduce<TodoItemNode[]>((accumulator, key) => {
            debugger;
            const value = obj[key];
            const node = new TodoItemNode();
            node.item = key;
            node.parent = parent;

            if (value != null) {
                if (typeof value === 'object') {
                    node.children = this.buildFileTree(value, level + 1, node);
                } else {
                    node.item = value;
                }
            }

            return accumulator.concat(node);
        }, []);
    }

    /** Add an item to to-do list */
    insertItem(parent: TodoItemNode, name: string, type: string = 'node') {
        if (parent.children) {
            if(type === 'folder')
                parent.children.push({ item: name, children: [], parent: parent } as TodoItemNode);
            else
                parent.children.push({ item: name, parent: parent } as TodoItemNode);
            this.dataChange.next(this.data);
        }
    }

    ejectItem(node: TodoItemNode) {
        let indx = node.parent.children.indexOf(node);
        node.parent.children.splice(indx, 1);
        this.dataChange.next(this.data);
    }

    ejectRoot(node: TodoItemNode) {
        debugger;
        let indx = this.data.indexOf(node);
        this.data.splice(indx, 1);
        this.dataChange.next(this.data);
    }


    /** Add an item to to-do list */
    insertTree(parent: TodoItemNode, name: string) {
        let item = new TodoItemNode('', true, []);
        this.data.push(item);
        this.dataChange.next(this.data);
    }

    updateItem(node: TodoItemNode, name: string) {
        node.item = name;
        this.dataChange.next(this.data);
    }
}