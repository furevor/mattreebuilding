import { TestBed, inject } from '@angular/core/testing';

import { ChecklistDatabaseService } from './checklist-database.service';

describe('ChecklistDatabaseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChecklistDatabaseService]
    });
  });

  it('should be created', inject([ChecklistDatabaseService], (service: ChecklistDatabaseService) => {
    expect(service).toBeTruthy();
  }));
});
