import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutsMenuComponent } from './layouts-menu.component';

describe('LayoutsMenuComponent', () => {
  let component: LayoutsMenuComponent;
  let fixture: ComponentFixture<LayoutsMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutsMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutsMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
