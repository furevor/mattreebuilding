import { Component, OnInit, Injectable } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { ChecklistDatabaseService, TodoItemFlatNode, TodoItemNode } from "../../_services/checklist-database.service";

import { DomSanitizer } from "@angular/platform-browser";
import { MatDialog } from '@angular/material';
import { DeleteConfirmDialogComponent } from '../delete-confirm-dialog/delete-confirm-dialog.component';



@Component({
    selector: 'layouts-menu',
    templateUrl: './layouts-menu.component.html',
    styleUrls: ['./layouts-menu.component.css']
})
export class LayoutsMenuComponent implements OnInit {


    ngOnInit() {

    }

    /** Map from flat node to nested node. This helps us finding the nested node to be modified */
    flatNodeMap = new Map<TodoItemFlatNode, TodoItemNode>();

    /** Map from nested node to flattened node. This helps us to keep the same object for selection */
    nestedNodeMap = new Map<TodoItemNode, TodoItemFlatNode>();

    /** A selected parent node to be inserted */
    selectedParent: TodoItemFlatNode | null = null;

    /** The new item's name */
    newItemName = '';

    treeControl: FlatTreeControl<TodoItemFlatNode>;

    treeFlattener: MatTreeFlattener<TodoItemNode, TodoItemFlatNode>;

    dataSource: MatTreeFlatDataSource<TodoItemNode, TodoItemFlatNode>;

    /** The selection for checklist */
    checklistSelection = new SelectionModel<TodoItemFlatNode>(true /* multiple */);

    constructor(private database: ChecklistDatabaseService, public dialog: MatDialog) {
        this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel,
            this.isExpandable, this.getChildren);
        this.treeControl = new FlatTreeControl<TodoItemFlatNode>(this.getLevel, this.isExpandable);
        this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

        database.dataChange.subscribe(data => {
            debugger;
            this.dataSource.data = data;
        });
    }

    confirmDelete(node: TodoItemFlatNode) {
        const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {});
    
        dialogRef.afterClosed().subscribe(result => {
          console.log(`Dialog result: ${result}`);
           if(result)
            this.deleteItem(node);
            
        });
      }

    getLevel = (node: TodoItemFlatNode) => node.level;

    isExpandable = (node: TodoItemFlatNode) => node.expandable;

    getChildren = (node: TodoItemNode): TodoItemNode[] => node.children;

    hasChild = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.expandable;

    canAdd = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.extendable;

    hasNoContent = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.item === '';

    /**
     * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
     */
    transformer = (node: TodoItemNode, level: number) => {
        const existingNode = this.nestedNodeMap.get(node);
        const flatNode = existingNode && existingNode.item === node.item
            ? existingNode
            : new TodoItemFlatNode();
        debugger;
        flatNode.item = node.item;
        flatNode.level = level;
        flatNode.expandable = !!node.children; //node.type === 'folder' ? true: false; //// && node.children.length > 0;
        flatNode.extendable = !!node.children && (node.children.length > 0) ? true : false;//node.type === 'folder';// node.extendable; //node.type === 'folder' ? true : false;
        this.flatNodeMap.set(flatNode, node);
        this.nestedNodeMap.set(node, flatNode);
        return flatNode;
    }

    /** Whether all the descendants of the node are selected */
    descendantsAllSelected(node: TodoItemFlatNode): boolean {
        const descendants = this.treeControl.getDescendants(node);
        return descendants.every(child => this.checklistSelection.isSelected(child));
    }

    /** Whether part of the descendants are selected */
    descendantsPartiallySelected(node: TodoItemFlatNode): boolean {
        const descendants = this.treeControl.getDescendants(node);
        const result = descendants.some(child => this.checklistSelection.isSelected(child));
        return result && !this.descendantsAllSelected(node);
    }

    /** Toggle the to-do item selection. Select/deselect all the descendants node */
    todoItemSelectionToggle(node: TodoItemFlatNode): void {
        this.checklistSelection.toggle(node);
        const descendants = this.treeControl.getDescendants(node);
        this.checklistSelection.isSelected(node)
            ? this.checklistSelection.select(...descendants)
            : this.checklistSelection.deselect(...descendants);
    }

    /** Select the category so we can insert the new item. */
    addNewItem(node: TodoItemFlatNode, type: string = 'node') {
        debugger;
        const parentNode = this.flatNodeMap.get(node);
        this.database.insertItem(parentNode!, '', type);
        this.treeControl.expand(node);
    }

    /** Save the node to database */
    saveNode(node: TodoItemFlatNode, itemValue: string) {
        debugger;
        const nestedNode = this.flatNodeMap.get(node);
        this.database.updateItem(nestedNode!, itemValue);
    }

    insertTree() {
        this.database.insertTree(null, '');
    }

    deleteItem(node: TodoItemFlatNode) {
        
        let parentNode = this.flatNodeMap.get(node);

        if(!!parentNode.parent) {
            this.database.ejectItem(parentNode);
        }
        else {
            this.database.ejectRoot(parentNode);
        }
    }

}