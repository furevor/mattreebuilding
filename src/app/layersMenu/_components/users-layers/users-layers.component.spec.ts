import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersLayersComponent } from './users-layers.component';

describe('UsersLayersComponent', () => {
  let component: UsersLayersComponent;
  let fixture: ComponentFixture<UsersLayersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersLayersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersLayersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
