import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'layers-menu-container',
    templateUrl: './layers-menu-container.component.html',
    styleUrls: ['./layers-menu-container.component.css']
})
export class LayersMenuContainerComponent implements OnInit {

    show = true;

    constructor() {
    }

    ngOnInit() {
    }

}
