import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayersMenuContainerComponent } from './layers-menu-container.component';

describe('LayersMenuContainerComponent', () => {
  let component: LayersMenuContainerComponent;
  let fixture: ComponentFixture<LayersMenuContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayersMenuContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayersMenuContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
