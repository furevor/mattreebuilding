import { LayersMenuModule } from './layers-menu.module';

describe('LayersMenuModule', () => {
  let layersMenuModule: LayersMenuModule;

  beforeEach(() => {
    layersMenuModule = new LayersMenuModule();
  });

  it('should create an instance', () => {
    expect(layersMenuModule).toBeTruthy();
  });
});
