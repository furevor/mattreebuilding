import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {LayersMenuModule} from './layersMenu/layers-menu.module'
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    LayersMenuModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
